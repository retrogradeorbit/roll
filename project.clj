(defproject roll "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [clj-ssh "0.5.11"]
                 [org.clojure/tools.cli "0.3.1"]
                 [bronsa/colorize "0.2.0"]
                 ]
  :plugins [[lein-bin "0.3.4"]
            [cider/cider-nrepl "0.8.2-SNAPSHOT"]]
  :bin {:name "roll"}
  :main ^:skip-aot roll.core
  :target-path "target/"
  :profiles {:uberjar {:aot :all}}
  :aliases {"server" ["run" "-m" "roll.server"]})
