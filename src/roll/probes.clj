(ns roll.probes
  (:use [clj-ssh.ssh])
  (:require [clojure.tools.cli :refer [parse-opts]]
            [colorize.ansi :refer [colorize]]
            [clojure.string :as string]
            [clojure.pprint :refer [pprint]]
            [roll.macros :refer [run]]
            [roll.utils :refer [string-to-keyword]])
  (:gen-class))

(defn lsb-release []
  (run "lsb_release -a"
    (into {}
          (for [[k v] (map #(string/split % #":\t")
                           (filter #(.contains % ":\t")
                                   (string/split out #"\n" )))]
            [(string-to-keyword k) v]))))

(defn uname []
  (run "uname -a"
    (let [[os name kernel & _] (string/split out #" ")]
      {
       :os os
       :name name
       :kernel kernel})))

(defn which-java []
  (run "which java"
    {:which-java (string/trim-newline out)}))

(defn java-info []
  (run "java -version"
    {:java (second
            (re-find #"\"([\d._]+)\""
                     (->
                      err
                      (string/split #"\n")
                      first)))}))

(defn os-info []
  (let [lsb (lsb-release)
        uname (uname)]
    (->> {
         :distro (string-to-keyword (:distributor-id lsb))
         :os (string-to-keyword (:os uname))
         }
         (into lsb)
         (into uname)
         (into (which-java))
         (into (java-info))
         (into {}))))

;; not really probes... where will we put these?
(defn chmod [perms path]
  (run (str "chmod '" perms "' '" path "'")
    {:out out
     :err err
     :exit exit}))

(defn md5 [path]
  (run (str "md5sum '" path "'")
    (-> out
        (string/split #"\s")
        first)))
