(ns roll.server
  (:require [clojure.edn :as edn])
  (:gen-class))

(defn -main
  "server mainline"
  [& args]
  (println (pr-str :ready))
  (loop []
    (let [command (read-line)
          ast (edn/read-string command)
          result (eval ast)]
      (if-not (nil? result)
        (do
          (println (pr-str {:result result}))
          (recur))))))
