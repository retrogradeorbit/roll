(ns roll.preflight
  (:use [clj-ssh.ssh])
  (:require [colorize.ansi :refer [colorize]]
            [clojure.edn :as edn]
            [clojure.java.io :as io]
            [roll.macros :refer [with-ssh run remote-run]]
            [roll.probes :refer [os-info chmod which-java java-info]]
            [roll.transport :refer [copy-to-remote]])
  (:import [java.io InputStream OutputStream PipedInputStream PipedOutputStream
            BufferedReader InputStreamReader])
  (:gen-class))

(defn package-update []
  (remote-run "apt-get update"))

(defn package-upgrade []
  (remote-run "apt-get -y upgrade"))

(defn install-java []
  (remote-run "apt-get -y install openjdk-7-jre"))

(defn error [str]
  (colorize str {:fg :red}))

(defn info [str]
  (colorize str {:fg :cyan}))

(defn ok [str]
  (colorize str {:fg :green}))

(defn setup [desc call success?]
  (print (info (str desc "... ")))
  (flush)
  (let [res (call)]
    (if (success? res)
      (println (info "ok."))
      (println (error "failed.")))
    res))

(defn preflight [opts]
  (let [remote-info (setup "Reading system info"
                           os-info
                           #(not (nil? (:os %))))]
    (when (:update opts)
      (setup "Updating remote package information"
             package-update
             #(= 0 (:exit %))))

    (when (:upgrade opts)
      (setup "Upgrading remote system packages"
             package-upgrade
             #(= 0 (:exit %))))

    (when (nil? (:java remote-info))
      (setup "Installing java on remote"
               (partial install-java remote-info)
               #(or
                 (nil? %)
                 (= 0 (:exit %)))))

    ;; dont do this every time
    (let [jar-copy (setup "Copying roll jar to remote"
                          (fn []
                            (copy-to-remote "target/roll" "/tmp/roll")
                            (chmod "755" "/tmp/roll"))
                          #(= 0 (:exit %)))]
      {:info remote-info
       :jar jar-copy})))
