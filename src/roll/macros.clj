(ns roll.macros
  (:use [clj-ssh.ssh])
  (:require [clojure.tools.cli :refer [parse-opts]]
            [colorize.ansi :refer [colorize]]
            [clojure.string :refer [split lower-case trim-newline]]
            [clojure.pprint :refer [pprint]])
  (:gen-class))


(defn ssh-connect [host args]
  (let [agent (ssh-agent {})
        session (session agent host args)]
    [session agent]))

(defonce ^:dynamic *session* nil)
(defonce ^:dynamic *ssh-agent* nil)

(defmacro with-ssh [user host port & body]
  `(let [[session# agent#]
         (ssh-connect
          ~host
          {:strict-host-key-checking :no    ;; or :ask or :yes
           :port ~port
           :username ~user})]
     (with-connection session#
       (binding [*session* session#
                 *agent* agent#]
         (do ~@body)))))

(macroexpand-1
 `(with-ssh "user" "localhost" 22 (install-stuff) (other-stuff)))

(defmacro run [command block]
  `(let [{:keys [~'exit ~'err ~'out]}
        (ssh *session* {:cmd ~command})]
    (cond
     (= ~'exit 0)
     ~block

     :else
     nil)))

(defmacro remote-run [command & in]
  `(ssh roll.macros/*session* {:cmd ~command :in ~in})
)
