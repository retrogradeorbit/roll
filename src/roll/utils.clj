(ns roll.utils
  (:use [clj-ssh.ssh])
  (:require [clojure.tools.cli :refer [parse-opts]]
            [colorize.ansi :refer [colorize]]
            [clojure.string :as string]
            [clojure.pprint :refer [pprint]]
            [roll.macros :refer [run]])
  (:gen-class))

(defn string-to-keyword [str]
  (-> str
      string/lower-case
      (string/replace #" " "-")
      keyword))

(defn install-jre [session]
  (run "apt-get install -y openjdk-7-jre" true))
