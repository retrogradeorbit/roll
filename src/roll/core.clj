(ns roll.core
  (:use [clj-ssh.ssh])
  (:require [clojure.tools.cli :refer [parse-opts]]
            [colorize.ansi :refer [colorize]]
            [clojure.string :as string]
            [clojure.pprint :refer [pprint]]
            [clojure.edn :as edn]
            [clojure.java.io :as io]
            [roll.macros :refer [with-ssh run remote-run]]
            [roll.probes :refer [os-info chmod which-java java-info]]
            [roll.transport :refer [copy-to-remote]]
            [roll.preflight :refer [preflight]])
  (:import [java.io InputStream OutputStream PipedInputStream PipedOutputStream
            BufferedReader InputStreamReader])
  (:gen-class))

(def version "0.0.1")

(def cli-options
  [
   ["-n" "--no-strict-checking" "Turn of strict host checking in ssh connection"]
   ["-h" "--help"]
   ["-V" "--version"]
   ["-p" "--port PORT" "Connect via ssh to this destination port"
    :default 22
    :parse-fn #(Integer/parseInt %)
    :validate [#(< 0 % 0x10000) "Must be a number between 0 and 65536"]
    ]
   ["-H" "--host HOST" "Connect to the specified host only"]
   ["-u" "--user USER" "Connect as the specified user"]
   ["-S" "--sudo" "Use sudo to execute superuser calls"]
   [nil "--update" "Update the remote package manager during preflight"]
   [nil "--upgrade" "Upgrade the remote packages during preflight"]
])

(defn error [msg]
  (colorize msg {:fg :red
                 :extra [:bold]}))

(defn >> [stream str]
  (doseq [c str] (.write stream (int c))))

(defn <<
  "reads a line and returns as a string"
  [stream]
  (first (line-seq (BufferedReader. (InputStreamReader. stream))))
)

(defn remote
  "run a command remotely and return its result"
  [in-stream out-stream code]
  (>> in-stream (prn-str code))
  (:result (edn/read-string (<< out-stream))))

(defn -main
  "mainline for roll command"
  [& args]
  (let [result (parse-opts args cli-options)
        opts (:options result)]
    (cond
     (:version opts)
     (println (str "Version: " version))

     (:help opts)
     (println (:summary result))

     :else
     (pprint (try
               (with-ssh (:user opts) (:host opts) (:port opts)
                 (let [remote-info (preflight opts)]
                   ;; setup ssh pipe in and out to roll server
                   (let [feed (PipedOutputStream.)
                         input (PipedInputStream. feed)
                         res (ssh roll.macros/*session*
                                  {:cmd "/usr/bin/java -cp /tmp/roll roll.server"
                                   :in input
                                   :out :stream})
                         channel (:channel res)
                         out-stream (:out-stream res)
                         err-stream (:err-stream res)]

                     ;; get :ready message
                     (println "<-" (<< out-stream))

                     ;; evaluate things on the remote server
                     (println "<<-" (remote feed out-stream
                                      '(for [i (range 100)] (* i i)))))))
               (catch com.jcraft.jsch.JSchException e
                 (error (.getMessage e))))))))
