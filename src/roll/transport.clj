(ns roll.transport
  (:use [clj-ssh.ssh])
  (:require [clojure.tools.cli :refer [parse-opts]]
            [colorize.ansi :refer [colorize]]
            [clojure.string :as string]
            [clojure.pprint :refer [pprint]]
            [roll.utils :refer [string-to-keyword]]))

(defn copy-to-remote [local remote]
  (let [channel (ssh-sftp roll.macros/*session*)]
    (with-channel-connection channel
      (sftp channel {} :put local remote))))

(defn remote-md5 [remote]
  )

(defn ensure-file-on-remote [local remote]
  )
